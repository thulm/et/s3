# 0. Was in Klausur dran kam

1. Eingangsvektor mit Zahlenwerten zu Ausgangsvektor mit gemittelten Zahlenwerten
   1. y[n] = (x[n-1] + x[n] + x[n+1])/3
   2. danach das gleiche Typ-unabhängig (typedef ...)  
2. liste nach name sortiert anlegen
3. auto (mercedes, bmw, bmw_2), überladene Konstruktoren, Kopierkonstruktor
4. kunde, pistenkunde (Vererbung von Oberklassen-Methode in Unterklasse usw.)
5. threads



## 1. C-Wiederholung & Basics (1-3)

| Code-Speicher    | Daten-Speicher                 | Stack-Speicher               | Heap-Speicher                                  |
| ---------------- | ------------------------------ | ---------------------------- | ---------------------------------------------- |
| Maschinenbefehle | globale + statissche Variablen | Parameter + lokale Variablen | Speicher vom Betriebssystem temporär anfordern |



| Variable       | Storage size | Value range                     | Platzhalter `printf`   | Platzhalter `scanf` |
| -------------- | ------------ | ------------------------------- | ---------------------- | ------------------- |
| string         |              |                                 | `%s`                   | \t → tab            |
| char           | 1 byte       | -128 to 127                     | `%c`                   | \\" → "             |
| unsigned char  | 1 byte       | 0 to 255                        |                        | \\' → '             |
| short          | 2 bytes      | $-2^{15}$ to $2^{15}-1$         |                        | %% → %              |
| unsigned short | 2 bytes      | $0$ to $2^{16}$                 |                        |                     |
| int            | 4 bytes      | $-2^{31}$ to $2^{31}-1$         | `%i`                   | `%i`                |
| unsigned int   | 4 bytes      | $0$ to $2^{32}$                 | `%u` (dez)  `%x` (hex) |                     |
| long           | 8 bytes      | $-2^{63}$ to $2^{63}-1$         |                        |                     |
| unsigned long  | 8 bytes      | $0$ to $2^{64}$                 |                        |                     |
| float          | 4 byte       | ±1.401e-45<br />bis +3.403e+38  | `%f`                   |                     |
| double         | 8 byte       | ±4.941e-324<br />bis 1.798e+308 | `%f`                   | `%lf`               |

<img src="../../Typora.assets/image-20200121120955865.png" alt="image-20200121120955865" style="zoom:50%;" />

| Bezeichnung              | Rechnung                                                     |
| ------------------------ | ------------------------------------------------------------ |
| Modulo / Rest            | 17 % 3 = 2        17 % -3 = 2        -17 % 3 = -2        -17 % -3 = -2 |
| normierte Gleitkommazahl | 384x10⁶ = 3,84x10⁸~10~ = 1,011011100011011E11100~2~          |
| Gleitkomma-Rechnung      | int tmp = 7.0/2.0  //tmp=3        float tmp = 7/2  //tmp=3.0<br />float tmp = 7.0/2  //tmp=3.5        ==(1/2 + 7.5) = 8.0 ???== |



| Standartfunktionen                                           | Funktion                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| exp(...);  /  sin(...)  /  cos(...)                          | e^...^                                                       |
| M_PI                                                         | $\pi$                                                        |
| log(...)  /  log10(...)  /  pow(x, y)                        | x^y^                                                         |
| abs(...);                                                    | Betrag von ...                                               |
| strlen(const char *str);                                     | Länge der Zeichenkette str[]={"..."}                         |
| ![image-20200121162830586](../../Typora.assets/image-20200121162830586.png) | ![image-20200121162730448](../../Typora.assets/image-20200121162730448.png) |
| strstr(const char *s1, const char *s2);                      | Zeiger auf Stelle, an der Zeichenfolge 2 in Zeichenfolge 1 vorkommt |
| strchr(const char *str, int c);                              | Zeiger auf erste Stelle, an der c in Zeichenfolge vorkommt   |
| num%2 == 0 (1)                                               | Zahl ist gerade (ungerade)                                   |



##### Zeiger

```c
int variable = 5;	// Variable erzeugen
int* pointer;		// Pointer erzeugen

pointer = &variable;		// Pointer ist nun die Adresse der Variable
*pointer = variable = 5;	// 5 ist der Inhalt bei der Adresse des Pointers
```



##### Zeichenketten

```c
char str[]={"Hallo\n"}
/*——————————————
 H a l l o \n \0		→ 7 Elemente
 ———————————————*/

char inpstr[100];
gets(inpstr);			//ermöglicht Konsolen-Eingabe mit Leerzeichen
printf("Gelesener String: %s\n", inpstr);
```





## 2. Dynamischer Speicher (3-13)



#### 2.1 [Speicher anfordern](https://youtu.be/_8-ht2AKyH4)

```c++
int a;	int* p;
//				C					//			C++				//			C++
p = (int*) malloc(sizeof(int));				p = new int;				p = new int[20];
free(p);									delete p;					delete[] p;
```



#### 2.2 Mehrdimensionales Array

```c++
int** arr = (int **) malloc(3*sizeof(int*));

arr[0] = (int *) malloc(3*sizeof(int));
arr[0][0] = 1;		arr[0][1] = 9;		arr[0][2] = 4;

arr[1] = (int *) malloc(2*sizeof(int));
arr[1][0] = 0;		arr[1][1] = 2;
```

<img src="../../Typora.assets/image-20210126181750271.png" alt="image-20210126181750271" style="zoom: 33%;" />





## 3. Zeichenketten- und Dateizugriffe (14-30)



#### 3.1 Zeichenketten lesen/schreiben

```C++
int sprintf (str, "%i ...", 2);	// in Zeichenkette schreiben
int sscanf (str, "%i", &num);		// aus Zeichenkette lesen → num=2
```



#### 3.2 Dateien lesen/schreiben

```c++
FILE* file ("datei.txt", "wert_in_tabelle");		// öffnen
fflush(file);										// Puffer in Datei leeren
```

| r                            | w                                | a                                             | rb    | wb    | ab    |
| ---------------------------- | -------------------------------- | --------------------------------------------- | ----- | ----- | ----- |
| lesen, wenn unmöglich = NULL | neue Datei anlegen/überschreiben | wenn datei vorhanden, ab Ende weiterschreiben | binär | binär | binär |





## 4. Listen (31-72)



#### 4.1 [Datenstrukturen](https://youtu.be/vcQIFT79_50)

```c++
struct Point{					struct Point{					struct {
    ...								...								...
};								} p1, p2;						} Point;

struct Point p1, p2;											Point p1, p2;
```



#### 4.2 [Dynamische Datenstrukturen](https://youtu.be/cAZ8CyDY56s)

```c++
struct Node{				// Node entspricht Listenelement
	int data;
    Node* next;
};

Node* head;					// Adresse des ersten Listenelements
head = NULL;				// da Liste am Anfang leer

Node* temp = (Node*) malloc(sizeof(Node));	// Erstellen des ersten Listenelements
// Alternativ: = new Node();				// temp enthält Adresse zu erstem LE 

(*temp).data = 2;			// *temp ist Inhalt der Adresse → Inhalt von erstem LE
// Alternativ: temp->data
(*temp).next = NULL;		// kein weiteres LE → Link darauf = NULL

head = temp;				// head ist Adresse von erstem LE
```

```c++
struct Node{
	int data;
    Node* next;
};
Node* head = NULL;

Node* temp = new Node();
temp->data = 2;
temp->next = NULL;
head = temp;

temp = new Node();
```



#### 4.3 [Liste Anzeigen](https://youtu.be/cAZ8CyDY56s?t=453)

```c++
void printlist(Node* head){
    Node* temp = head;
    
    while(temp != NULL){
        printf("%i", temp->data);
        temp = temp->next;
    }
}
```





## 5. Funktionen und Module (73-96)



#### 5.1 Static

| Intern               | Extern               | Lokal          | Global                |
| -------------------- | -------------------- | -------------- | --------------------- |
| (innerhalb Funktion) | (außerhalb Funktion) | (nur in Modul) | (in mehreren Modulen) |

```c++
void func(){			int i=0;				static int i=0;			int i=0;
    int i=0;
}						void func(){...};		void func(){...};		...
```

> hinter static kann auch Funktion stehen → gleiche Bedeutung



#### 5.2 Extern

```c++
//fileA.cpp
int i = 42; // declaration and definition

//fileB.cpp
extern int i;  // declaration only. same as i in FileA

//fileC.cpp
extern int i;  // declaration only. same as i in FileA

//fileD.cpp
int i = 43; // LNK2005! 'i' already has a definition.
extern int i = 43; // same error (extern is ignored on definitions)
```

```c++
//fileA.cpp
extern const int i = 42; // extern const definition

//fileB.cpp
extern const int i;  // declaration only. same as i in FileA
```



#### 5.3 Strukturen Funktionen zuordnen

| Speicherplatz reservieren | Speicherplatz freigeben |
| ------------------------- | ----------------------- |
| `Node *n = new Node`      | `delete n`              |
| `int *arr = new int[10]`  | `delete [] arr`         |



#### 5.4 [Stack](https://www.youtube.com/watch?v=MuwxQ2IB8lQ&list=PL2_aWCzGMAwI3W_JlcBbtYTwiQSsOTa6P&index=16)

```c++
struct Node{
    int data;
    Node* next;
}

Node* top = NULL;

void push(int x){
    Node* temp = new Node();		// neuer Element wird erzeugt
    temp->data = x;					// Zahl wird in Element geschrieben
    temp->next = top;				// Element zeigt auf vormals oberstes Element
    top = temp;						// neues Element wird zu oberstem Element
}

void pop(int* x){
    Node* temp = new Node();
    temp = top;
    top = top->next;
    *x = temp->data;
    free(temp)
}
```





## 7. Objektorientierte Programmierung (109-172)

| Abstraktion                                                  | Kapselung                                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| - Modul über möglichst einfache Schnittstelle benutzbar<br />- kein Wissen über innere Funktion nötig | - Zusammensetzung aus Variablen & Methoden<br />- Zugriff auf Variablen kann nur über definierte Funktionen |





## 8. Überladen, Container und Templates (173-218)



#### 8.1 Referenzparameter

```c++
class Sorter {
public: void sort2Numbers(int &num1, int &num2);
};

void Sorter::sort2Numbers(int &num1, int &num2) {
	int tmp;
	if (num1 > num2) {
		tmp = num1;
		num1 = num2;
		num2 = tmp;
	}
}
```



#### 8.2 Funktionen/Methoden überladen

- gleicher Funktionsname
- Identifikation durch Art und Reihenfolge der Parameter
- Datentyp spielt keine Rolle

```c++
class Bulb{
    public:
    	Bulb();								// Default Konstruktor
    	Bulb(char* color);					// später kann Startwert übergeben
    	Bulb(char* color = "white");		// Startwert kann optional eingegeben werden
    private:
    	char* BulbColor;
    	const int size;						// Konstanten in Konstruktor nur durch
}											// :size(wert) möglich, nicht {size = wert}

Bulb::Bulb(char* color){	BulbColor = color;	}			//	\
Bulb::Bulb(char* color){	this->BulbColor = color;	}	//	 | → entspricht
Bulb::Bulb(char* color):BulbColor(color){ ... }				//	/
// Allgemein: i=1 entspricht i(1)
```



#### 8.3 Ein- und Ausgabeströme

```c++
// Ausgabe von 34,56789
cout << setprecision(4);						// 34,67

cout << setiosflags(std::ios_base::fixed);
cout << setprecision(4);						// 34,5679

cout << "y: " << setw(10) << y << endl;			// 34,5679

int x;	double y;	cin >> x;	cin >> y;		// einlesen
```



#### 8.4 Templates

```c++
template<typename T1, typename T2>

void add(T1& z1, T2& z2){z1 += (T1) z2}
```

```c++
template<class T>
class Klasse{
public:
    T get();	// get value
private:
    T* array;
    T value
}

template<class T>
T Klasse<T>::get(){ 	return value;	}

int main(){		Klasse<int> objekt;		}
```





## 9. Klassen und Zeichenketten (219-289)

```c++
#include <string>

string str = "String";
int i = 1;
char c

cin << str;						// Liest bis zum ersten Leerzeichen
getline(cin, str);				// Liest bis zum Zeilenende
getline(cin, str, 'z');			// Liest bis zum angegebenen Zeichen

str.length();					// Länge des Strings
str.empty();					// true, falls String leer, sonst false
str.clear();					// String leeren
str.swap(str2);					// Inhalt zweier Strings vertauschen

str[3] / str.at(3);				// Zeichen an Stelle 3, 'at' gibt Fehler wenn kein Zeichen

toupper(c); / tolower(c);		// Zeichen zu Groß-/Kleinbuchstaben wandeln

str.find('t', 0); / rfind('g');	// 1./letztes Vorkommen von 'c' in str, Startwert optional

str.replace(2, 3, "eet");		// str wird ab 2. Zeichen mit "eet" (Länge 3) ersetzt

str.substr(2,3);				// Teilstring: beginnt ab 2. Position, optional 3 lang

str = to_string(i);				// str enthält nun "1"
```



#### 9.1 Stream: String bearbeiten

```c++
#include <sstream>

int x = 10;

ostringstream strStream;			/* entspricht */		char str[100];
strstream << "x = " << x;									sprintf(str, "x = %i", x);
string outStr = strStream.str();
```



#### 9.2 Stream: Datei bearbeiten

```c++
#include <fstream>

ifstream readFile(filepath);	// Einlesen aus einer Datei
ofstream writeFile(filepath);	// Schreiben in eine Datei
fstram readwriteFile(filepath);	// Schreiben in und Lesen aus einer Datei

writeFile.open(filename, mode);	// mode: ios::binary (Binärmodus), ios::app (Inhalte an
    							// Dateiende), ios::trunc (Inhalte vor schreiben löschen)
    
writeFile << "Ausgabetext.\n";
writeFile.close();

int main() {
	string line;
	ifstream myfile("example.txt");
	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			cout << line << '\n';
		}
		myfile.close();
	}
	else cout << "Unable to open file";
	return 0;
}
```



#### 9.3 Objekte löschen

```c++
class Vektor{
    Vektor(double fvec[], int dim){			// Konstruktor
        vec = new double[dim];
    }
    ~Vektor(){								// Destruktor
        delete [] vec;
    }
};

double farr[] = {1, 2, 3};
Vektor *v = new Vektor(farr, 3);
delete v;									// Löst Destruktoraufruf aus
```



#### 9.4 Objekte vergleichen

```c++
bool operator==(Lightbulb &lhs, Lightbulb &rhs) {
	if (strcmp(lhs.getColor(), rhs.getColor()) == 0)
		return true;
	else
		return false;
}

Lightbulb wLb, cLb("red");
if (wLb == cLb)
	cout << "wLb und cLb: gleichartige Objekte\n";
else
	cout << "wLb und cLb: verschiedenartige Objekte\n";		// da wLb=weiß verschieden
```



#### 9.5 <<-Operator überladen

```c++
ostream& operator<<(ostream &strm, Klasse &k) {
	return strm << "Eigenschaft: " << k.eigenschaft;
}

cout << klasse << endl;
```





## 10. Komposition und Vererbung (290-342)



#### 10.1 Komposition

```c++
#ifndef CAR_H
#define CAR_H
#include "engine.h"
#include "wheel.h"
#include "door.h"

class Car {
public:
	Engine engine;
	Wheel wheel[4];
	Door left, right;
};
#endif // CAR_H
```

```c++
#include "car.h"
Car::Car():left("Left door"), right("Right door"){}
```



#### 10.2 Vererbung

|                 | public | private           | protected                           |
| --------------- | ------ | ----------------- | ----------------------------------- |
| Zugriff hat ... | jeder  | nur Klasse selbst | nur Klasse selbst oder Unterklassen |

```cpp
class A { Oberklasse }		// Private Komponenten werden nicht vererbt
class B : public A { }		// Bis auf die privaten kann auf alle Komponenten von A zu-
							// gegriffen werden. Diese behalten in B ihr Attribut von A
class C : protected A { }	// public- und protected-Komponenten von A sind in C protected
class D : private A { }		// public- und protected-Komponenten von A sind in D private

Oberklasse* o = new Unterklasse; /* ok */	Unterklasse* u = new Oberklasse // verboten
    
Oberklasse o;	Unterklasse u;		u = o;		// verboten
    
void Unterklasse::methode(){	// durch Unterklasse überschriebene Methode von
    Oberklasse::methode();		// Oberklasse kann folgendermaßen aufgerufen werden
}

Oberklasse* o = (Oberklasse*) u;	// Objekt o hat kann auf alle Methoden zugreifen, die
									// nur in Unterklasse deklariert wurden
```



#### 10.3 Klassen-Attribute

```c++
class Klasse final: public Oberklasse {...}		// keine Unterklassen von Klasse möglich

class Klasse{									// Statische Variable/Funktion wird über
    static int var=1;							// Klasse aufgerufen, nicht über daraus
    static int add(int n1, int n2){				// erzeugtes konkretes Objekt
        return n1 + n2; }
}													// statische ... sind einfach in
													// Unterklasse verfügbar
cout << Klasse::var++ << Klasse::add(2,4) << endl;
```



#### 10.4 ==Schnittstellen==-Klasse (virtuell / abstrakt)

```c++
class VirtuelleKlasse{
    virtual void methode() = 0;		// Methode muss bei Unterklasse überschrieben werden
}

class Unterklasse : public VirtuelleKlasse {
    void methode();				// muss in cpp-Datei definiert werden
}
```



## 11. Dynamische Polymorphie (343-380)



## 12. Grafische Bedienoberflächen (381-426)



#### 12.1 Button,	Check Box,	Radio Button,	Label

```c++
QPushButton b("OK");	QPushButton* b = new QPushButton("OK");		// Button anlegen
b.setText("My Button");	  						// Button beschriften
b.setEnabled(false);	b.setVisible(false);	// Button (de)aktivieren / (un)sichtbar


QCheckBox cbChin("Chin");	// verschieden viele Checkboxen können selektiert werden

QRadioButton rb("Dog");		// es kann nur einer von mehreren Buttons aktiv sein

QLabel label("Label 1");	// Textfeld zur Beschreibung
```



#### 12.2 Textfeld

```c++
QLineEdit lineEdit("QLineEdit Widget");		QLabel label("Label 1");
lineEdit.setText("Neuer Text");				label.setText(lineEdit.text());

setText()	text()	// Text schreiben / lesen

QTextEdit	QPlainTextEdit	// Mehrzeiliger Editor für (HTML) / normalen Text
```



#### 12.3 Layout Manager

```c++
QPushButton b1, b2, b3, b4, b5, b6;

QVBoxLayout vlayout;			QHBoxLayout hlayout;			// .h
vlayout.addWidget(&b1);			hlayout.addWidget(&b1);			// .cpp
... /* b2 - b5 */				... /* b2 - b5 */
```

​					<img src="../../Typora.assets/image.NZLWW0.png" alt="image.NZLWW0" style="zoom:50%;" />									<img src="../../Typora.assets/image.92YVW0.png" alt="image.92YVW0" style="zoom:50%;" />

```c++
QPushButton b1, b2, b3, b4, b5, b6;

// .h								.cpp
QWidget centralWidget;			h1layout.addWidget(&b1);	// -b3
QVBoxLayout vlayout;			h2layout.addWidget(&b4);	// -b6
QHBoxLayout h1layout;			vlayout.addLayout(&h1layout);
QHBoxLayout h2layout;			vlayout.addLayout(&h2layout);

						centralWidget.setLayout(&vlayout);
						this->setCentralWidget(&centralWidget);
						this->setWindowTitle("Layout Example");
```

<img src="../../Typora.assets/image.793SW0.png" alt="image.793SW0" style="zoom:67%;" />



<img src="../../Typora.assets/image-20210127225732347.png" alt="image-20210127225732347" style="zoom: 67%;" />

![unchecked](../../Typora.assets/unchecked.png)							![checked](../../Typora.assets/checked.png)





## 13. Threads (427-450)



#### 13.1 Join

```c++
sleep_for(chrono::milliseconds(500));		// 500ms warten , bspw in for-Schleife

int main(){					// Thread wird parallel ausgeführt, könnte auch länger als
    thread t(function);		// main brauchen
    t.join();			// wartet, bis thread abgeschlossen, dann läuft Progr. weiter
    return 0;
}
```



#### 13.2 Mutex

```c++
mutex m;

m.lock();
cout << "etwas ...";		// Ausgabe kann nur von diesem Thread benutzt werden
m.unlock();
```



#### 13.3 Detach

```c++
void child(){ ... }

void parent(){
	thread t(child);
    t.detach();			// child kann jetzt auch als parent-thread brauchen
}
```


